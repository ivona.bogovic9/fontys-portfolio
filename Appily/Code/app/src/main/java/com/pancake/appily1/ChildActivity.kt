package com.pancake.appily1

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.media.AudioManager
import android.media.SoundPool
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.math.sqrt

@Suppress("DEPRECATION")
class ChildActivity : AppCompatActivity() {

    //variables for shake sensor
    private var sensorManager: SensorManager? = null
    private var acceleration = 0f
    private var currentAcceleration = 0f
    private var lastAcceleration = 0f

    //variables for flip animation
    private lateinit var flip_anim: AnimatorSet
    private lateinit var back_anim: AnimatorSet
    private var isfront = true

    //variables for sound effects
    private var soundPool: SoundPool? = null
    private var sound01 = 1
    private var sound02 = 2
    private var sound03 = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_child)

        soundPool = SoundPool(6, AudioManager.STREAM_MUSIC, 0)
        sound01 = soundPool!!.load(baseContext, R.raw.bling, 1)
        sound02 = soundPool!!.load(baseContext,R.raw.fail, 1)
        sound03 = soundPool!!.load(baseContext,R.raw.hooray, 1)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        Objects.requireNonNull(sensorManager)!!.registerListener(sensorListener, sensorManager!!
            .getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL)
        acceleration = 10f
        currentAcceleration = SensorManager.GRAVITY_EARTH
        lastAcceleration = SensorManager.GRAVITY_EARTH

        //bottom buttons and their preliminary interactions
        val c_profile = findViewById<ImageView>(R.id.c_account)
        val c_info = findViewById<ImageView>(R.id.c_info)
        c_profile.setOnClickListener { Toast.makeText(applicationContext, "You are now logged in as a child!", Toast.LENGTH_SHORT).show() }
        c_info.setOnClickListener { Toast.makeText(applicationContext, "If you do not follow through, you cannot expect others to do it too!", Toast.LENGTH_SHORT).show() }

        //variables to access cards
        val face_card = findViewById<ImageView>(R.id.c_front)
        val back_card = findViewById<ImageView>(R.id.c_back)

        // setting the camera distance from the view
        val scale = applicationContext.resources.displayMetrics.density;
        face_card.cameraDistance = 8000 * scale
        back_card.cameraDistance = 8000 * scale

        // setting the front animation
        flip_anim = AnimatorInflater.loadAnimator(applicationContext, R.animator.rotation) as AnimatorSet

        //setting the back animation
        back_anim = AnimatorInflater.loadAnimator(applicationContext, R.animator.back_flip) as AnimatorSet
    }
    private val sensorListener: SensorEventListener = object : SensorEventListener {
        override fun onSensorChanged(event: SensorEvent) {
            val x = event.values[0]
            val y = event.values[1]
            val z = event.values[2]

            val face_card = findViewById<ImageView>(R.id.c_front)
            val back_card = findViewById<ImageView>(R.id.c_back)

            val activity = resources.getStringArray(R.array.c_tasks)
            val c_task = findViewById<TextView>(R.id.c_task)
            val c_skip = findViewById<TextView>(R.id.c_skip)
            val c_done = findViewById<TextView>(R.id.c_done)

            lastAcceleration = currentAcceleration
            currentAcceleration = sqrt((x * x + y * y + z * z).toDouble()).toFloat()
            val delta: Float = currentAcceleration - lastAcceleration
            acceleration = acceleration * 0.9f + delta

            if (acceleration > 12 && isfront) {
                playFlipSound()
                flip_anim.setTarget(face_card)
                back_anim.setTarget(back_card)
                c_task.text = activity.random().toString()
                c_skip.visibility = View.VISIBLE
                c_done.visibility = View.VISIBLE
                c_task.visibility = View.VISIBLE
                flip_anim.start()
                back_anim.start()
                isfront = false
                c_skip.setOnClickListener {
                    playSkipSound()
                    flip_anim.setTarget(back_card)
                    back_anim.setTarget(face_card)
                    c_skip.visibility = View.INVISIBLE
                    c_done.visibility = View.INVISIBLE
                    c_task.visibility = View.INVISIBLE
                    back_anim.start()
                    flip_anim.start()
                    isfront = true
                }
                c_done.setOnClickListener {
                    playDoneSound()
                    flip_anim.setTarget(back_card)
                    back_anim.setTarget(face_card)
                    c_skip.visibility = View.INVISIBLE
                    c_done.visibility = View.INVISIBLE
                    c_task.visibility = View.INVISIBLE
                    back_anim.start()
                    flip_anim.start()
                    isfront = true
                }

                val v = (getSystemService(Context.VIBRATOR_SERVICE) as Vibrator)
                // Vibrate for 500 milliseconds
                // Vibrate for 500 milliseconds
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    v.vibrate(VibrationEffect.createOneShot(500,
                            VibrationEffect.DEFAULT_AMPLITUDE))
                }
                else {
                    v.vibrate(500)
                }

            }
        }
        override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}
    }

    private fun playFlipSound() {
        val prefs = getSharedPreferences("userSoundSettings", MODE_PRIVATE)
        val soundSettings = prefs.getBoolean("userSoundSettings", true)

        if (soundSettings){
            soundPool?.play(sound01, 1F, 1F, 0, 0, 1F)
        }
    }

    private fun playSkipSound() {
        val prefs = getSharedPreferences("userSoundSettings", MODE_PRIVATE)
        val soundSettings = prefs.getBoolean("userSoundSettings", true)

        if (soundSettings) {
            soundPool?.play(sound02, 1F, 1F, 0, 0, 1F)
        }
    }

    private fun playDoneSound() {
        val prefs = getSharedPreferences("userSoundSettings", MODE_PRIVATE)
        val soundSettings = prefs.getBoolean("userSoundSettings", true)

        if (soundSettings){
            soundPool?.play(sound03, 1F, 1F, 0, 0, 1F)
        }
    }

    override fun onResume() {
        sensorManager?.registerListener(sensorListener, sensorManager!!.getDefaultSensor(
            Sensor .TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL
        )
        super.onResume()
    }
    override fun onPause() {
        sensorManager!!.unregisterListener(sensorListener)
        super.onPause()
    }
    fun accessSettings(view: View) {
        val imageView = findViewById<ImageView>(R.id.c_settings)
        imageView.setOnClickListener {
            val intent = Intent(this, Settings::class.java)
            startActivity(intent)
        }
    }
}