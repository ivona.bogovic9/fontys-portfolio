package com.pancake.appily1

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val m_info = findViewById<ImageView>(R.id.m_info)
        m_info.setOnClickListener { Toast.makeText(applicationContext, "App-ily consists of responsibilities and rewards for an endless family fun!", Toast.LENGTH_SHORT).show() }
    }

    /** Called when the user taps the image button */
    fun accessParentView(view: View) {
        val imageView = findViewById<ImageView>(R.id.parents)
        imageView.setOnClickListener {
            val intent = Intent(this, ParentActivity::class.java)
            startActivity(intent)
        }
    }
    fun accessChildView(view: View) {
        val imageView = findViewById<ImageView>(R.id.kids)
        imageView.setOnClickListener {
            val intent = Intent(this, ChildActivity::class.java)
            startActivity(intent)
        }
    }
    fun accessSettings(view: View) {
        val imageView = findViewById<ImageView>(R.id.m_settings)
        imageView.setOnClickListener {
            val intent = Intent(this, Settings::class.java)
            startActivity(intent)
        }
    }
}