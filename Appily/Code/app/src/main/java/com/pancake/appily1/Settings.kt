package com.pancake.appily1

import android.os.Bundle
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

@Suppress("DEPRECATION")
class Settings : AppCompatActivity() {

    val PREF_NAME = "userSoundSettings"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        val soundSetting = findViewById<Switch>(R.id.sound)
        val sharedPreferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        soundSetting.isChecked = sharedPreferences.getBoolean(PREF_NAME, true)
        soundSetting.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                editor.putBoolean(PREF_NAME, true)
                Toast.makeText(this, "Sound Effects ON", Toast.LENGTH_SHORT).show()
            } else {
                editor.putBoolean(PREF_NAME, false)
                Toast.makeText(this, "Sound Effects Off", Toast.LENGTH_SHORT).show()
            }
            editor.apply()
        }
    }
}