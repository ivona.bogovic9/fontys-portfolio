//
//  ContentView.swift
//  BraveBee
//
//  Created by Edita on 09/03/2021.
//

import SwiftUI

struct FearItem: Identifiable {
  let id = UUID()
  let name: String
  let description: String
  let moreinfo: String
}

class Fears: ObservableObject {
@Published var listOfFears: [FearItem] = [
   FearItem(
     name: "Acrophobia",
     description: "Acrophobia is an extreme or irrational fear or phobia of heights, especially when one is not particularly high up.",
    moreinfo: "https://en.wikipedia.org/wiki/Acrophobia"),
   FearItem(
     name: "Arachnophobia",
     description: "Arachnophobia is an intense and irrational fear of spiders and other arachnids such as scorpions.",
    moreinfo: "https://en.wikipedia.org/wiki/Arachnophobia"),
    FearItem(
      name: "Claustrophobia",
      description: "Claustrophobia is the irrational fear of confined spaces. People affected by claustrophobia will often go out of their way to avoid confined spaces, such as lifts, tunnels, tube trains and public toilets.",
     moreinfo: "https://en.wikipedia.org/wiki/Claustrophobia"),
    FearItem(
      name: "Philophobia",
      description: "Philophobia is the fear of falling in love. The risk is usually when a person has confronted any emotional turmoil relating to love but also can be a chronic phobia.",
    moreinfo: "https://en.wikipedia.org/wiki/Philophobia_(fear)"),
    FearItem(
      name: "Scopophobia",
      description: "Scopophobia is an excessive fear of being stared at.",
     moreinfo: "https://en.wikipedia.org/wiki/Scopophobia")
]
}

class MyFears: ObservableObject {
    @Published var phobias = [String]()
}

struct ContentView: View {
    init() {
          UIToolbar.appearance().barTintColor = UIColor.black
      }
    @State private var revealDetails = false
        var body: some View {
            NavigationView{
                Color.black
                    .ignoresSafeArea(.all) // Ignore just for the color
                        .overlay(
            VStack(alignment: .leading){
                Text("Brave Bee").bold().padding().font(.largeTitle).foregroundColor(.yellow)
                Spacer().frame(height: 250)
                Section{
                    DisclosureGroup("         Join the hive", isExpanded: $revealDetails) {
                        Text("Welcome to the hive. Here you can battle your fears. Beat the challenges to become more comfortable with your phobias.")
                    }.frame(width: 180)
                NavigationLink(destination: FearListView()){
                    Text("Login").padding()
                        .frame(width: 200, height: 50, alignment: .center)
                        .background(Color("BeeGray"))
                        .foregroundColor(.yellow)
                        .clipShape(Capsule())
                }
                NavigationLink(
                        destination: FearListView())
                         {
                        Text("Register").padding()
                                .frame(width: 200, height: 50, alignment: .center)
                                .foregroundColor(.yellow)
                                .background(Color("BeeGray"))
                                .clipShape(Capsule())
                        }
                }.foregroundColor(.yellow)
            })
            .navigationTitle("")
            .navigationBarHidden(true)
                    .navigationViewStyle(StackNavigationViewStyle())
                    .toolbar {
                        ToolbarItemGroup(placement: .bottomBar) {
                            HStack{
                            NavigationLink(
                                destination: FearListView())
                                 {Image(systemName: "list.dash")
                            }
                                Spacer()
                                NavigationLink(
                                    destination: SettingsView())
                                     {
                                    Image(systemName: "gear")
                            }
                            }.padding().frame(width: 220)
                        }
                    }.accentColor(.gray)
    }
 }
}
    
struct FearListView: View {
    @State private var searchText = ""
    @State private var showCancelButton: Bool = false
    @ObservedObject var fears = Fears()

    var body: some View {
        VStack{
            Text("Choose a fear").bold().foregroundColor(.yellow).padding().font(.largeTitle).frame(alignment: .leading)
            HStack{
                HStack{
                    Image(systemName: "magnifyingglass")
                                    TextField("search", text: $searchText, onEditingChanged: { isEditing in
                                        self.showCancelButton = true
                                    }, onCommit: {
                                        print("onCommit")
                                    }).foregroundColor(.yellow)

                                    Button(action: {
                                        self.searchText = ""
                                    }) {
                                        Image(systemName: "xmark.circle.fill").opacity(searchText == "" ? 0 : 1)
                                    }
                }.padding(EdgeInsets(top: 8, leading: 6, bottom: 8, trailing: 6))
                .foregroundColor(.yellow)
                .background(Color(.secondarySystemBackground))
                .cornerRadius(10.0)
                if showCancelButton  {
                                Button("Cancel") {
                                        UIApplication.shared.endEditing(true) // this must be placed before the other commands here
                                        self.searchText = ""
                                        self.showCancelButton = false
                                }
                                .foregroundColor(Color(.yellow))
                            }
            }.padding(.horizontal)
            .navigationBarHidden(showCancelButton).foregroundColor(.yellow)
            Text("Tap on the fear to read about it").foregroundColor(.yellow).frame(width: 380, alignment: .leading)
            
            List(fears.listOfFears.filter(
                    {searchText.isEmpty ? true:$0.name.contains(searchText)
            }))
            {
                fearItem in
                                NavigationLink(destination: FearDetailsView(fearItem: fearItem)) {
                                HStack {
                                    Text(fearItem.name)
                                        .foregroundColor(.gray)
                                        .font(.headline)
                                }.padding(7)
                              }
            }
        }
        .navigationBarHidden(true)
                .navigationViewStyle(StackNavigationViewStyle())
                .toolbar {
                    ToolbarItemGroup(placement: .bottomBar) {
                        HStack{
                        NavigationLink(
                            destination: FearListView())
                             {Image(systemName: "list.dash")
                        }
                            Spacer()
                            NavigationLink(
                                destination: SettingsView())
                                 {
                                Image(systemName: "gear")
                        }
                        }.padding().frame(width: 220)
                    }
                }.accentColor(.gray)
    }
}

struct FearDetailsView: View {

  let fearItem: FearItem
  @ObservedObject var myFears = MyFears()
  
  var body: some View {
    VStack{
        Text("   " + fearItem.name)
          .font(.largeTitle)
          .bold()
            .foregroundColor(.yellow)
      Text(fearItem.description)
        .foregroundColor(.gray)
        .padding()
        Link("    Read more", destination: URL(string: fearItem.moreinfo)!).foregroundColor(.yellow)
        NavigationLink(destination: MyListView()) {
            Text("Sting it!").frame(width: 200, height: 50, alignment: .center)
                .background(Color("BeeGray"))
                .foregroundColor(.yellow)
                .clipShape(Capsule())
        }.padding()
        Spacer()
                .gesture(TapGesture().onEnded{onAdd()
        })
    }
    .navigationBarHidden(false)
            .toolbar {
                ToolbarItemGroup(placement: .bottomBar) {
                    HStack{
                    NavigationLink(
                        destination: FearListView())
                         {Image(systemName: "list.dash")
                    }
                        Spacer()
                        NavigationLink(
                            destination: SettingsView())
                             {
                            Image(systemName: "gear")
                    }
                    }.padding().frame(width: 220)
                }
            }.accentColor(.gray)
  }
    func onAdd() {
        myFears.phobias.append(fearItem.name)
    }
}

struct MyListView: View {
   @ObservedObject var fears = Fears()
   @ObservedObject var myFears = MyFears()
    @State var lista = ["Acrophobia","Philophobia"]
    var body: some View {
        VStack(alignment: .center) {
            Text("My personal fears").bold().padding().font(.largeTitle).foregroundColor(.yellow)
            List(lista, id: \.self) {
            fear in
                NavigationLink(destination: WeekOverviewView()) {
            HStack {
                    Text(fear)
                        .foregroundColor(.gray)
                        .font(.headline)
                        }.padding(7)
                }
            }
        }
        .navigationBarHidden(false)
                .navigationViewStyle(StackNavigationViewStyle())
                .toolbar {
                    ToolbarItemGroup(placement: .bottomBar) {
                        HStack{
                        NavigationLink(
                            destination: FearListView())
                             {Image(systemName: "list.dash")
                        }
                            Spacer()
                            NavigationLink(
                                destination: SettingsView())
                                 {
                                Image(systemName: "gear")
                        }
                        }.padding().frame(width: 220)
                    }
                }.accentColor(.gray)
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

extension UIApplication {
    func endEditing(_ force: Bool) {
        self.windows
            .filter{$0.isKeyWindow}
            .first?
            .endEditing(force)
    }
}
