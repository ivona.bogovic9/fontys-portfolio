//
//  FearInfoView.swift
//  BraveBee
//
//  Created by Edita on 11/03/2021.
//

import SwiftUI

struct FearInfoView: View {
    var body: some View {
        VStack(alignment: .leading){
            Text("Acrophobia").padding().font(.largeTitle).foregroundColor(.yellow)
            Text("Acrophobia describes an intense fear of heights that can cause significant anxiety and panic. Some research suggests acrophobia may be one of the most common phobias. This distress is generally strong enough to affect your daily life.").padding().foregroundColor(.gray)
            Link("Read more", destination: URL(string: "https://en.wikipedia.org/wiki/Acrophobia")!).padding().font(.caption)
            NavigationLink(destination: MyFearListView()){
                Text("Sting it").padding()
                    .frame(width: 200, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .foregroundColor(.yellow)
                    .background(Color("BeeGray"))
                    .clipShape(Capsule())
            }
        }.frame(minWidth: 100, maxWidth: .infinity, minHeight: 100, maxHeight: .infinity)
        .background(Color.black)
        .edgesIgnoringSafeArea(.all)
    }
}

struct FearInfoView_Previews: PreviewProvider {
    static var previews: some View {
        FearInfoView().environment(\.colorScheme, .dark)
    }
}
