//
//  SettingsView.swift
//  BraveBee
//
//  Created by Ivona on 14/03/2021.
//

import SwiftUI

struct SettingsView: View {
    @State private var notifications = false
    var body: some View {
        Color.black
                .edgesIgnoringSafeArea(.all) // Ignore just for the color
                .overlay(
                    VStack{
                        Text("Settings").bold().foregroundColor(.yellow).padding().frame(width: 380, alignment: .leading).font(.largeTitle)
                Text("Profile").frame(width: 330, alignment: .leading)
                List{
                    HStack{
                        Text("jane.doe@gmail.com").listRowBackground(Color.black)
                        Button("Change email"){
                            
                        }.frame(width: 150, height: 20, alignment: .center)
                        .background(Color("BeeGray"))
                        .foregroundColor(.yellow)
                        .clipShape(Capsule())
                    }.frame(width: 320, height: 20, alignment: .trailing)
                    HStack{
                        Text("*********                     ").listRowBackground(Color.black)
                        Button("Change password"){
                            
                        }.frame(width: 150, height: 20, alignment: .center)
                        .background(Color("BeeGray"))
                        .foregroundColor(.yellow)
                        .clipShape(Capsule())
                    }.frame(width: 320, height: 20, alignment: .trailing)
                }.frame(height: 100, alignment: .center).background(Color.black)
            HStack{
                Text("Notifications").padding()
                Toggle("", isOn: $notifications)
                    .toggleStyle(SwitchToggleStyle(tint: .yellow))
            }
            NavigationLink(destination: ContentView()){
                Text("Sign out").padding()
                    .frame(width: 200, height: 50, alignment: .center)
                    .foregroundColor(.yellow)
                    .background(Color("BeeGray"))
                    .clipShape(Capsule())
            }
            }
                    .frame(width: 359, height: 700, alignment: .top)
            .padding()
            .foregroundColor(.yellow)
            .background(Color.black)
            .cornerRadius(10.0))
            .navigationBarHidden(false)
                    .navigationViewStyle(StackNavigationViewStyle())
                    .toolbar {
                        ToolbarItemGroup(placement: .bottomBar) {
                            HStack{
                            NavigationLink(
                                destination: FearListView())
                                 {Image(systemName: "list.dash")
                            }
                                Spacer()
                                NavigationLink(
                                    destination: SettingsView())
                                     {
                                    Image(systemName: "gear")
                            }
                            }.padding().frame(width: 220)
                        }
                    }.accentColor(.gray)
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
