//
//  ChallengeView.swift
//  BraveBee
//
//  Created by Ivona on 11/03/2021.
//

import SwiftUI

struct ChallengeView: View {
    @State private var showingAlert = false
    var body: some View {
        Color.black
            .ignoresSafeArea(.all) // Ignore just for the color
                .overlay(
        VStack(alignment: .center){
            Text("  Week 1 Challenge").font(.largeTitle).bold()
            Text("    Acrophobia").foregroundColor(.gray)
            Text("Visualise going on an observation tower.").padding()
            Text("How to do it?").padding().font(.title)
            Text("With eyes closed, visualise in detail all of the security precautions that will surround you in the situation you’re entering. Feel the solidity of the stairs beneath your feet; feel the comforting grasp of the handrail; in your mind’s eye, stretch your arm into the space in front of you. Let these feelings sink into your subconscious, where they can help you when your heart starts pounding. Do this every night before going to sleep.").padding().foregroundColor(.gray)
            Section{
                Text("For low impact").padding().font(.title)}
            Text("Do this exercise at least 4 times this week to complete the challenge.").padding().foregroundColor(.gray)
            Button("Complete") {
                        showingAlert = true
                    }.frame(width: 200, height: 50, alignment: .center)
            .background(Color("BeeGray"))
            .foregroundColor(.yellow)
            .clipShape(Capsule())
                    .alert(isPresented: $showingAlert) {
                        Alert(title: Text("Good Job!"), message: Text("You have completed the challenge of the day!"), dismissButton: .default(Text("Bzzz"))
                    )
                    }
        }.foregroundColor(.yellow)
        .frame(width: 359, height: 700, alignment: .top)
                .toolbar {
                    ToolbarItemGroup(placement: .bottomBar) {
                        HStack{
                        NavigationLink(
                            destination: FearListView())
                             {Image(systemName: "list.dash")
                        }
                            Spacer()
                            NavigationLink(
                                destination: SettingsView())
                                 {
                                Image(systemName: "gear")
                        }
                        }.padding().frame(width: 220)
                    }
                }.accentColor(.gray)
    )
    }
}
struct ChallengeView_Previews: PreviewProvider {
    static var previews: some View {
        ChallengeView()
            .environment(\.colorScheme, .dark)    }
}
