//
//  MyFearList.swift
//  BraveBee
//
//  Created by Edita on 10/03/2021.
//

import SwiftUI

struct MyFearListView: View {
    let fears = ["Acrophobia", "Claustrophobia"]
    var body: some View {
        Color.black
            .ignoresSafeArea(.all) // Ignore just for the color
                .overlay(
        VStack{
            Text("My fear list").foregroundColor(.yellow).padding().frame(width: 380, alignment: .leading).font(.largeTitle)
        List (fears, id: \.self) {
            Text($0)
        }
        NavigationLink(destination: FearSearchView()){
            Image(systemName: "text.badge.plus").resizable().frame(width: 30, height: 30)}.frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .trailing).foregroundColor(.yellow).font(.title)
        }
                )
    }
}

struct MyFearListView_Previews: PreviewProvider {
    static var previews: some View {
        MyFearListView()
            .environment(\.colorScheme, .dark)
    }
}

