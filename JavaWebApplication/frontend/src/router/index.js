import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'HelloWorld',
        component: () =>
            import ('../components/HelloWorld.vue')
    },
    {
        path: '/register',
        name: 'register',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import ('../views/Signup.vue')
    },
    {
        path: '/login',
        name: 'login',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import ('../views/Login.vue')
    },
    {
        path: '/profile',
        name: 'profile',
        // lazy-loaded
        component: () =>
            import ('../views/Profile.vue')
    },
    {
        path: '/user',
        name: 'user',
        // lazy-loaded
        component: () =>
            import ('../views/BoardUser.vue')
    },
    {
        path: '/character',
        name: 'character',
        // lazy-loaded
        component: () =>
            import ('../views/CreateCharacter.vue')
    },
    {
        path: '/story',
        name: 'story',
        // lazy-loaded
        component: () =>
            import ('../views/Story.vue')
    },
    {
        path: '/questions',
        name: 'questions',
        // lazy-loaded
        component: () =>
            import ('../views/Questions.vue')
    },
    {
        path: '/charEditor',
        name: 'charEditor',
        // lazy-loaded
        component: () =>
            import ('../views/CharacterEditor.vue')
    }
]

// router.beforeEach((to, from, next) => {
//     const publicPages = ['/login', '/register', '/'];
//     const authRequired = !publicPages.includes(to.path);
//     const loggedIn = localStorage.getItem('user');

//     // trying to access a restricted page + not logged in
//     // redirect to login page
//     if (authRequired && !loggedIn) {
//         next('/login');
//     } else {
//         next();
//     }
// });

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router