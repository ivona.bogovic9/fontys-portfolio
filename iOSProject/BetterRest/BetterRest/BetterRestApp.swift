//
//  BetterRestApp.swift
//  BetterRest
//
//  Created by Ivona on 26/02/2021.
//

import SwiftUI

@main
struct BetterRestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
