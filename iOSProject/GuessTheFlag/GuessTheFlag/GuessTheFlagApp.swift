//
//  GuessTheFlagApp.swift
//  GuessTheFlag
//
//  Created by Ivona on 23/02/2021.
//

import SwiftUI

@main
struct GuessTheFlagApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
