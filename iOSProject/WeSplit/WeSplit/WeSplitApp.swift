//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Ivona on 22/02/2021.
//

import SwiftUI

@main
struct WeSplitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
