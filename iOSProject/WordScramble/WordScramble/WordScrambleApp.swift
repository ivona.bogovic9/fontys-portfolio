//
//  WordScrambleApp.swift
//  WordScramble
//
//  Created by Ivona on 02/03/2021.
//

import SwiftUI

@main
struct WordScrambleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
